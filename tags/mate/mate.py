#!/usr/bin/env python3

import psycopg2
import sys
import random
import chess
import chess.uci
import time
import logging
import copy

pathToEngine = "Stockfish/stockfish_6_x64"
#pathToEngine = "toga2/toga2"
checkTime = 400

mates = { 1: "Mate in 1",
          2: "Mate in 2",
          3: "Mate in 3",
          4: "Mate in 4",
          5: "Mate in 5",
          6: "Mate in 6",
          7: "Mate in 7",
          8: "Mate in 8",
          9: "Mate in 9",
          10: "Mate in 10" }

def saveTag(blunder_id,mate):
    if(mate > 10):
        print("Mate is to far. skipping")
        return

    cur = psql.cursor()

    #try:
    mate_name = mates[mate]
    query = """insert into blunder_tags(blunder_id,tag_id) values ('%s',(select id from blunder_tag_type where name = '%s' ));""" % (blunder_id,mate_name)
    cur.execute(query)
    psql.commit()
    #except:
    #    print("Insertion error. May be this record already exist")  

def getScore(engine, timeToThink):
    engine.isready()
    engine.go(infinite = True, async_callback=True)

    time.sleep(timeToThink / 1000)

    tries = 0
    score = None
    pv = None
    while score is None or pv is None:
        with handler:
            if "score" in handler.info and "pv" in handler.info and 1 in handler.info["score"] and 1 in handler.info["pv"]:
                score = handler.info["score"][1]
                pv = handler.info["pv"][1]
                #print(handler.info)
            else:
                tries += 1
                time.sleep(timeToThink / 1000 / 5)
                if tries >= 100:
                    score = None
                    pv = None
                    break

    engine.stop()
    engine.isready()

    return pv, score

def scanBlunder(index, blunder):
    try:
        time.sleep(50/1000)
        blunder_id, fen_before, blunder_move = blunder

        engine.isready()
        engine.ucinewgame()

        board = chess.Board(fen_before)
        board.push_san(blunder_move)

        engine.isready()
        engine.position(copy.deepcopy(board))

        pv,score = getScore(engine, checkTime);
        if score is None:
            sys.exit()
            
        mate = score.mate
        if mate is None:
            print("(%s/%s) %s: Nothing" % (index, limit, blunder_id,))
        else:
            print("(%s/%s) %s: Mate in %s - %s" % (index, limit, blunder_id, mate, board.fen()))
            saveTag(blunder_id, mate)
    except KeyboardInterrupt:
        print("Bye!")
        sys.exit()
    except psycopg2.IntegrityError:
        logger.error("%s parsing failed. Integrity error" % (blunder_id,))
    except Exception as e:
        logger.error("%s parsing failed" % (blunder_id,))
        print("Some exception happened...")

def startEngine():
    global engine
    global handler

    engine = chess.uci.popen_engine(pathToEngine)
    engine.uci()
    handler = chess.uci.InfoHandler()
    engine.info_handlers.append(handler)
    engine.isready()

    #engine.setoption({'MultiPV': 1})
    #engine.isready()

    if not engine.is_alive():
        return False

    return True

def startPostgre():
    global psql
    try:
        psql = psycopg2.connect("dbname='chessdb' user='postgres'")
        print("Connected to db")
    except:
        print("I am unable to connect to the database")

def initLogger():
    global logger

    logger = logging.getLogger('mate-collector')
    hdlr = logging.FileHandler("/tmp/mate/mate-%s-%s.log" % (offset, limit,))
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.WARNING)

def main(args):
    global offset
    offset = sys.argv[1]
    global limit 
    limit = sys.argv[2]

    print("Starting worker: offset: %s, limit: %s" % (offset, limit))

    if not startEngine():
        print("Can't connect to engine")
        return
    print("Started engine %s" % (pathToEngine,))

    startPostgre()
    initLogger()

    cur = psql.cursor()
    try:
        cur.execute("""SELECT id, fen_before, blunder_move from blunders order by id limit %s offset %s""" % (limit, offset,))
    except:
        print("I can't get data from server")

    #index = 0
    #blunder = cur.fetchone()
    #while blunder:
    #    scanBlunder(index, blunder)
    #    blunder = cur.fetchone()
    #    index = index + 1

    blunders = cur.fetchall()
    for index, blunder in enumerate(blunders):
        scanBlunder(index, blunder)

    print("Finishing worker successfully!: offset: %s, limit: %s" % (offset, limit))

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print('Bye!')

