#!/usr/bin/env python3

import psycopg2
import sys
import random
import chess
import chess.uci
import time
import logging
import copy

from chess.gaviota import Endgamekey


def saveTag(blunder_id,tag_id):
    cur = psql.cursor()

    query = """insert into blunder_tags(blunder_id,tag_id) values ('%s',%s);""" % (blunder_id,tag_id)
    #print(query)
    cur.execute(query)
    psql.commit()

def isPromoting(turn, whitePawns, blackPawns):
    whitePromoting = chess.SquareSet(whitePawns & (chess.BB_RANK_7))
    whiteBlocking = chess.shift_up(whitePromoting)

    blackPromoting = chess.SquareSet(blackPawns & (chess.BB_RANK_2))
    blackBlocking = chess.shift_down(blackPromoting)

    if (turn == chess.WHITE) and (whiteBlocking & (~blackPawns)):
        return True;

    if (turn == chess.BLACK) and (blackBlocking & (~whitePawns)):
        return True;

    return False

def getAttacksCount(board):
     valuable_attack = lambda m: (board.is_capture(m)) and (board.piece_type_at(m.from_square) not in [chess.PAWN, chess.KING])
     myAttacks = list(filter(valuable_attack, chess.LegalMoveGenerator(board)))
     board.turn = not board.turn
     hisAttacks = list(filter(valuable_attack, chess.LegalMoveGenerator(board)))
     board.turn = not board.turn
     #print ("(%s %s)" % (myAttacks, hisAttacks))
     return len(myAttacks) + len(hisAttacks)

def getSilentCount(board):
     count = 0
     not_capture = lambda m: (not board.is_capture(m)) and (board.piece_type_at(m.from_square) not in [chess.PAWN, chess.KING])
     count += len(list(filter(not_capture, chess.PseudoLegalMoveGenerator(board))))
     board.turn = not board.turn
     count += len(list(filter(not_capture, chess.PseudoLegalMoveGenerator(board))))
     board.turn = not board.turn
     return count

def pawnSelfBlockCount(whitePawns, blackPawns):
    whitePromoting = chess.SquareSet(whitePawns)
    whiteBlocking = chess.shift_up(whitePromoting)

    countBlock = len(whiteBlocking & blackPawns)
    #print(whiteBlocking & blackPawns)
    return countBlock


def scanBlunder(index, blunder):
    blunder_id, fen_before, blunder_move = blunder

    board = chess.Board(fen_before)
    board.push_san(blunder_move)

    whitePawns = board.pieces(chess.PAWN, chess.WHITE)
    blackPawns = board.pieces(chess.PAWN, chess.BLACK)

    whiteKnights = board.pieces(chess.KNIGHT, chess.WHITE)
    blackKnights = board.pieces(chess.KNIGHT, chess.BLACK)
    whiteBishops = board.pieces(chess.BISHOP, chess.WHITE)
    blackBishops = board.pieces(chess.BISHOP, chess.BLACK)
    whiteRooks = board.pieces(chess.ROOK, chess.WHITE)
    blackRooks = board.pieces(chess.ROOK, chess.BLACK)
    whiteQueens = board.pieces(chess.QUEEN, chess.WHITE)
    blackQueens = board.pieces(chess.QUEEN, chess.BLACK)

    knights = whiteKnights | blackKnights
    bishops = whiteBishops | blackBishops
    rooks = whiteRooks | blackRooks
    queens = whiteQueens | blackQueens

    totalPawns = whitePawns | blackPawns
    totalStrong = knights | bishops | rooks | queens

    countPawns = len(list(totalPawns))
    countStrong = len(list(totalStrong))

    #attacksCount = getAttacksCount(board)
    #if(attacksCount >= 14):
    #    Aggressive.append(blunder_id)
    #    #print("Aggressive: %s" % (blunder_id,))  # OK

    if countStrong >= 6:
        if(isPromoting(board.turn, whitePawns, blackPawns)): #promoting pawn
            Promoting.append(blunder_id)
            print("Promoting: %s" % (blunder_id,))

    if countStrong >= 6:
        silentMovesPerPiece = getSilentCount(board) / countStrong
        #print(silentMovesPerPiece)
        if(pawnSelfBlockCount(whitePawns, blackPawns) >= 5):
            Closed.append(blunder_id)
            print("Closed: %s - %s" % (blunder_id,silentMovesPerPiece))

    if(countStrong + countPawns <= 3):  #5 pieces on the board or less   OK
        EndGame5.append(blunder_id)
        print("5 piece Endgame: %s" % (blunder_id,))

    if(countPawns >= 15 and countStrong >= 7 * 2 - 1): # at most 1 pawn and 1 piece are gone  OK
        Opening.append(blunder_id)
        print("Opening: %s" % (blunder_id,))

def storeResults():
    print("Total positions scanned: %s" % (limit,))
    print("5 pieces Engame positions found: %s (%d%%)" % (len(EndGame5),100*len(EndGame5)/int(limit)))
    print("Opening positions found: %s (%d%%)" % (len(Opening),100*len(Opening)/int(limit)))
    print("Promotions found: %s (%d%%)" % (len(Promoting),100*len(Promoting)/int(limit)))
    #print("Aggressive found: %s (%d%%)" % (len(Aggressive),100*len(Aggressive)/int(limit)))
    print("Closed found: %s (%d%%)" % (len(Closed),100*len(Closed)/int(limit)))

    for blunder_id in Opening:
        saveTag(blunder_id, 11)

    for blunder_id in Closed:
        saveTag(blunder_id, 12)

    for blunder_id in EndGame5:
        saveTag(blunder_id, 13)

    for blunder_id in Promoting:
        saveTag(blunder_id, 14)


def startPostgre():
    global psql
    try:
        psql = psycopg2.connect("dbname='chessdb' user='postgres'")
        print("Connected to db")
    except:
        print("I am unable to connect to the database")

def initLogger():
    global logger

    logger = logging.getLogger('mate-collector')
    hdlr = logging.FileHandler("/tmp/phase/mate-%s-%s.log" % (offset, limit,))
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.WARNING)

def initBuffers():
    global EndGame5, Opening, Promoting, Aggressive, Closed

    EndGame5 = []
    Opening = []
    Promoting = []
    Aggressive = []
    Closed = []

def main(args):
    global offset
    offset = sys.argv[1]
    global limit 
    limit = sys.argv[2]

    print("Starting worker: offset: %s, limit: %s" % (offset, limit))

    startPostgre()
    initLogger()
    initBuffers()

    cur = psql.cursor()
    try:
        cur.execute("""SELECT id, fen_before, blunder_move from blunders order by id limit %s offset %s""" % (limit, offset,))
    except:
        print("I can't get data from server")

    blunders = cur.fetchall()
    for index, blunder in enumerate(blunders):
        scanBlunder(index, blunder)

    storeResults()

    print("Finishing worker successfully!: offset: %s, limit: %s" % (offset, limit))

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print('Bye!')

