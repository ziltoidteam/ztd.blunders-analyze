#!/usr/bin/env python3

import psycopg2
import sys
import random
import chess
import chess.uci
import time
import logging
import copy

from chess.gaviota import Endgamekey


def saveTag(blunder_id,mate):
    if(mate > 10):
        print("Mate is to far. skipping")
        return

    cur = psql.cursor()

    #mate_name = mates[mate]
    #query = """insert into blunder_tags(blunder_id,tag_id) values ('%s',(select id from blunder_tag_type where name = '%s' ));""" % (blunder_id,mate_name)
    #cur.execute(query)
    #psql.commit()

def isPromoting(turn, whitePawns, blackPawns):
    whitePromoting = chess.SquareSet(whitePawns & (chess.BB_RANK_6 | chess.BB_RANK_7))
    whiteBlocking = chess.shift_up(whitePromoting)

    blackPromoting = chess.SquareSet(blackPawns & (chess.BB_RANK_2 | chess.BB_RANK_3))
    blackBlocking = chess.shift_down(blackPromoting)

    if (turn == chess.WHITE) and (whiteBlocking & (~blackPawns)):
        return True;

    if (turn == chess.BLACK) and (blackBlocking & (~whitePawns)):
        return True;

    return False

def getAttacksCount(board):
     count = 0
     count += len(list(filter(board.is_capture, chess.LegalMoveGenerator(board))))
     board.turn = not board.turn
     count += len(list(filter(board.is_capture, chess.LegalMoveGenerator(board))))
     board.turn = not board.turn
     return count

def getSilentCount(board):
     count = 0
     not_capture = lambda m: (not board.is_capture(m)) and (board.piece_type_at(m.from_square) not in [chess.PAWN, chess.KING])
     count += len(list(filter(not_capture, chess.PseudoLegalMoveGenerator(board))))
     board.turn = not board.turn
     count += len(list(filter(not_capture, chess.PseudoLegalMoveGenerator(board))))
     board.turn = not board.turn
     return count

def pawnSelfBlockCount(whitePawns, blackPawns):
    whitePromoting = chess.SquareSet(whitePawns)
    whiteBlocking = chess.shift_up(whitePromoting)

    countBlock = len(whiteBlocking & blackPawns)
    #print(whiteBlocking & blackPawns)
    return countBlock


def scanBlunder(index, blunder):
    blunder_id, fen_before, blunder_move = blunder

    board = chess.Board(fen_before)
    board.push_san(blunder_move)

    whitePawns = board.pieces(chess.PAWN, chess.WHITE)
    blackPawns = board.pieces(chess.PAWN, chess.BLACK)

    whiteKnights = board.pieces(chess.KNIGHT, chess.WHITE)
    blackKnights = board.pieces(chess.KNIGHT, chess.BLACK)
    whiteBishops = board.pieces(chess.BISHOP, chess.WHITE)
    blackBishops = board.pieces(chess.BISHOP, chess.BLACK)
    whiteRooks = board.pieces(chess.ROOK, chess.WHITE)
    blackRooks = board.pieces(chess.ROOK, chess.BLACK)
    whiteQueens = board.pieces(chess.QUEEN, chess.WHITE)
    blackQueens = board.pieces(chess.QUEEN, chess.BLACK)

    knights = whiteKnights | blackKnights
    bishops = whiteBishops | blackBishops
    rooks = whiteRooks | blackRooks
    queens = whiteQueens | blackQueens

    totalPawns = whitePawns | blackPawns
    totalStrong = knights | bishops | rooks | queens

    countPawns = len(list(totalPawns))
    countStrong = len(list(totalStrong))

    attacksCount = getAttacksCount(board)
    if(attacksCount >= 14):
        Aggressive.append(blunder_id)
        #print("Aggressive: %s" % (blunder_id,))

    if(isPromoting(board.turn, whitePawns, blackPawns)):
        Promoting.append(blunder_id)
        #print("Promoting: %s" % (blunder_id,))

    if countStrong > 6:
        silentMovesPerPiece = getSilentCount(board) / countStrong
        #print(silentMovesPerPiece)
        if(pawnSelfBlockCount(whitePawns, blackPawns) > 4):
            Closed.append(blunder_id)
            #print("Closed: %s - %s" % (blunder_id,getSilentCount(board)))

    if(countStrong <= 2):
        EndGame.append(blunder_id)
        print("Deep Endgame: %s" % (blunder_id,))
        return

    if(countPawns >= 14 and countStrong >= 7 * 2 - 2):
        Opening.append(blunder_id)
        #print("Opening: %s" % (blunder_id,))
        return

def storeResults():
    print("Total positions scanned: %s" % (limit,))
    print("Engame positions found: %s (%d%%)" % (len(EndGame),100*len(EndGame)/int(limit)))
    print("Opening positions found: %s (%d%%)" % (len(Opening),100*len(Opening)/int(limit)))
    print("Promotions found: %s (%d%%)" % (len(Promoting),100*len(Promoting)/int(limit)))
    print("Aggressive found: %s (%d%%)" % (len(Aggressive),100*len(Aggressive)/int(limit)))
    print("Closed found: %s (%d%%)" % (len(Closed),100*len(Closed)/int(limit)))


def startPostgre():
    global psql
    try:
        psql = psycopg2.connect("dbname='chessdb' user='postgres'")
        print("Connected to db")
    except:
        print("I am unable to connect to the database")

def initLogger():
    global logger

    logger = logging.getLogger('mate-collector')
    hdlr = logging.FileHandler("/tmp/phase/mate-%s-%s.log" % (offset, limit,))
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.WARNING)

def initBuffers():
    global EndGame, Opening, Promoting, Aggressive, Closed

    EndGame = []
    Opening = []
    Promoting = []
    Aggressive = []
    Closed = []

def main(args):
    global offset
    offset = sys.argv[1]
    global limit 
    limit = sys.argv[2]

    print("Starting worker: offset: %s, limit: %s" % (offset, limit))

    startPostgre()
    initLogger()
    initBuffers()

    cur = psql.cursor()
    try:
        cur.execute("""SELECT id, fen_before, blunder_move from blunders order by id limit %s offset %s""" % (limit, offset,))
    except:
        print("I can't get data from server")

    blunders = cur.fetchall()
    for index, blunder in enumerate(blunders):
        scanBlunder(index, blunder)

    storeResults()

    print("Finishing worker successfully!: offset: %s, limit: %s" % (offset, limit))

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print('Bye!')

