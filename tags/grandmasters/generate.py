#!/usr/bin/env python3

import psycopg2
import sys
import random
import chess
import chess.uci
import time
import logging
import copy

def saveTag(blunder_id):
    cur = psql.cursor()

    try:
      query = """insert into blunder_tags(blunder_id,type_id) values ('%s',(select id from blunder_tag_type where name = 'Grandmasters' ));""" % (blunder_id,)
      cur.execute(query)
      psql.commit()
    except:
        print("Insertion error. May be this record already exist")  

def scanBlunder(index, blunder):
    try:
        blunder_id, white_elo, black_elo = blunder
        print("%s %s - (%s, %s)" % (
          index,
          blunder_id,
          white_elo,
          black_elo
	)) 
        
        saveTag(blunder_id)
    except KeyboardInterrupt:
        print("Bye!")
        sys.exit()
    except psycopg2.IntegrityError:
        logger.error("%s parsing failed. Integrity error" % (blunder_id,))
    except Exception as e:
        logger.error("%s parsing failed" % (blunder_id,))
        print("Some exception happened...")

def startPostgre():
    global psql
    try:
        print("Connecting to db...")
        psql = psycopg2.connect("host='blunders-master.clotqfqonef0.eu-west-1.rds.amazonaws.com' dbname='chessdb' user='postgres' password='chessdb'")
        print("Connected to db")
    except:
        print("I am unable to connect to the database")

def main(args):
    startPostgre()
    
    cur = psql.cursor()
    try:
        cur.execute("""SELECT b.id, g.white_elo, g.black_elo from blunders as b inner join games as g on b.game_id = g.id where white_elo >= 2500 and black_elo >= 2500""")
    except:
        print("I can't get data from server")

    blunders = cur.fetchall()
    for index, blunder in enumerate(blunders):
        scanBlunder(index, blunder)

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print('Bye!')

