
import chess
import chess.uci
import time
#import logging

class Mate0Exception(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class Engine:
    def __init__(self, path):
        #logging.basicConfig(level=logging.DEBUG)
        self.engine = chess.uci.popen_engine(path)
        self.engine.uci()
        self.engine.isready()

        # chess.uci.InfoHandler()
        self.handler = chess.uci.InfoHandler()
        self.engine.info_handlers.append(self.handler)

        self.board = chess.Board()

        if not self.engine.is_alive():
            raise Exception("Engine failed to start")

        print("Name: %s" % self.engine.name)
        print("Author: %s" % self.engine.author)

    def new(self, fen):
        self.engine.ucinewgame()
        self.engine.isready()

        self.board = chess.Board(fen)
        self.engine.position(self.board)
        self.engine.isready()

    def is_game_over(self):
        return self.board.is_game_over()

    def __cloneScore(self, score):
        return { "cp": score.cp, "mate": score.mate }

    def __filterMove(self, index, old):
        pv = self.handler.info['pv']
        score = self.handler.info['score']

        if pv == {} or score == {}:
            return old

        if index not in pv or index not in score:
            return old

        resMove = self.board.san(pv[index][0])
        resScore = self.__cloneScore(score[index])

        if(resScore == {"cp": None, "mate":0}):
            print("--- Mate 0 Upperbound ---, old is %s" % old)
            print(self.handler.info)
            raise Mate0Exception("Mate 0 Error!!!!")

        return { 'move': resMove, 'score': resScore }

    def think(self, timeToThink):
        self.engine.setoption({'MultiPV': 2})
        self.engine.isready()

        moveCandidate   =  { 'move': None, 'score': None }
        secondCandidate =  { 'move': None, 'score': None }

        # We want to get first 2 move candidates to know the gap between them
        promise = self.engine.go(movetime=timeToThink, async_callback=True)

        promise.result()
        moveCandidate = self.__filterMove(1, moveCandidate)
        secondCandidate = self.__filterMove(2, secondCandidate)

        return moveCandidate, secondCandidate

    def forced(self, move):
        first = { 'move': move, 'score': None }
        second = { 'move': None, 'score': None }

        return first, second

    def move(self, move):
        move_int = self.board.parse_san(move)
        if move_int not in self.board.legal_moves:
            raise Exception("Illegal move in this position")

        self.board.push(move_int)
        self.engine.position(self.board)
        self.engine.isready()

    def unmove(self):
        self.board.pop()
        self.engine.position(self.board)
        self.engine.isready()

    def __enter__(self):
        return self

    def __exit__(self, exception, _1, _2):
        print("Quit")
        self.engine.quit()
