
from analyze.engine import Engine

infinity = 100000

class PVBuilder:
    def __init__(self, engine_path):
        self.engine = Engine(engine_path)
        self.time = 1000
        self.blunderGap = 240

    def setTime(self, time):
        self.time = time

    def setBlunderGap(self, gap):
        self.blunderGap = gap

    def __calculateElo(self, control, moves):
        complexity = 0
        for controlElement in control:
            if controlElement['gap'] is not None:
                complexity += 1 / controlElement['gap'];

        elo = int(1200 + complexity * 100000) + (len(moves) - 1) * 30

        return elo

    def __createControlArray(self, blunderMove):
        max_index = 50 # constant must be big enough
        control = [
            {
                'forced': None,
                'own': index % 2 != 0,
                'gap': None
            }
            for index in range(max_index)
        ]
        control[0]['forced'] = blunderMove

        return control

    def __scoreToInt(self, score):
        if score['mate'] is not None:
            if score['mate'] > 0:
                return +infinity
            elif score['mate'] < 0:
                return -infinity

        return score['cp']

    def __compareScores(self, move, second):
        return self.__scoreToInt(move) - self.__scoreToInt(second)

    def __cutLine(self, controlElement, move, second):
        # Don't cut if this is opponent move
        if(controlElement['own'] == False):
            return False

        # Only only move, can't calculate gap
        if(second != {'move': None, 'score': None}):
            gap = self.__compareScores(move['score'], second['score'])
            # Sometimes gap is negative. Probably because of asynchronious
            gap = gap if gap >= 0 else -gap
            print(gap)
            if gap <= self.blunderGap:
                return True
            controlElement['gap'] = gap

        # If both scores are too far from 0, it's not interesting
        if (not move['score'] is None) and (not second['score'] is None):
            score1 = move['score']['cp']
            score2 = second['score']['cp']
            if (not score1 is None) and (not score2 is None):
                if (abs(score1) >= 2000) and (abs(score2) >= 2000) and score1 * score2 > 0:
                    return True

        # by default dont cut
        return False

    def start(self, fenBefore, blunderMove):
        self.engine.new(fenBefore)

        control = self.__createControlArray(blunderMove)

        moves = []
        for index, element in enumerate(control):
            if self.engine.is_game_over():
                break

            forcedMove = element['forced']
            if forcedMove is not None:
                move, second = self.engine.forced(forcedMove)
            else:
                move, second = self.engine.think(self.time)

            print(move, second)

            if(self.__cutLine(element, move, second)):
                break;

            moves.append({
                '1': move,
                '2': second
            })
            self.engine.move(move['move'])

        if (control[len(moves) - 1]['own'] == False):
            moves = moves[:-1]

        elo = self.__calculateElo(control, moves)

        return moves, elo

    def __enter__(self):
        return self

    def __exit__(self, exception, _1, _2):
        pass
