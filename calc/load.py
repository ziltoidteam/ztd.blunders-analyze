#!/usr/bin/env python3

import logging
import os
import sys
import analyze
import db
import json

def getAnalyzed():
    with db.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT  bf.blunder_id,
                    bf.forced_line,
                    bf.elo,
                    bf.meta
            FROM blunder_fixes as bf
            """
        )

        fixes = connection.cursor.fetchall()
        return fixes

def getCurrentLine(blunder_id):
    with db.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT  b.id,
                    b.forced_line,
                    b.elo,
                    b.meta
            FROM blunders as b
            WHERE b.id = %s
            """, (blunder_id,)
        )

        if connection.cursor.rowcount != 1:
            raise Exception('Blunder cant be found')

        (_, forced_line, elo, meta) = connection.cursor.fetchone()
        return forced_line, elo, meta

def update(blunder_id, forced_line, elo, enabled, meta):
    with db.PostgreConnection('w') as connection:
        connection.cursor.execute("""
            UPDATE blunders
            SET forced_line = %s,
                elo = %s,
                enabled = %s,
                meta = %s
            WHERE id = %s
        """, (forced_line, elo, enabled, json.dumps(meta), blunder_id)
        )

def enable(blunder_id, enabled, meta):
    with db.PostgreConnection('w') as connection:
        connection.cursor.execute("""
            UPDATE blunders
            SET enabled = %s,
                meta = %s
            WHERE id = %s
        """, (enabled, json.dumps(meta), blunder_id)
        )

def close_fix(blunder_id):
    with db.PostgreConnection('w') as connection:
        connection.cursor.execute("""
            DELETE FROM blunder_fixes
            WHERE blunder_id = %s
        """, (blunder_id, )
        )

def saveBlunder(blunder_id, forced_line, elo, meta):
    old_forced_line, old_elo, old_meta = getCurrentLine(blunder_id)

    enabled = len(forced_line) != 0

    changed = (old_forced_line != forced_line)

    meta['elo'] = elo

    if enabled:
        if changed:
            print("%s: CHANGED %s(%s) -> %s(%s)" % (blunder_id, old_forced_line, old_elo, forced_line, elo))
            update(blunder_id, forced_line, elo, 1, meta)
        else:
            print("%s: REMAINS %s" % (blunder_id, old_forced_line))
            update(blunder_id, forced_line, elo, 1, meta)
            #enable(blunder_id, 1, meta)
    else:
        print("%s: DISABLED" % (blunder_id,))
        enable(blunder_id, 0, meta) # Dont change line if blunder disabled

    close_fix(blunder_id)

def main():
    fixes = getAnalyzed()
    print ("%s blunders to load" % len(fixes))
    for fix in fixes:
        blunder_id, forced_line, elo, meta = fix

        # Elo is limited to 3000
        if elo > 3000:
            elo = 3000

        saveBlunder(blunder_id, forced_line, elo, meta)
    print("Done")


if __name__ == '__main__':
    main()
