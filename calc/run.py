#!/usr/bin/env python3

import json
import logging
import os
import sys
import analyze
import db
import time

def as_array(l):
    l2str = ','.join('"{}"'.format(x) for x in l)
    return '{{{}}}'.format(l2str)

def getBlunders(timePerPosition):
    with db.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT  b.id,
                    b.fen_before,
                    b.blunder_move,
                    b.forced_line,
                    b.elo
            FROM blunders as b
            LEFT JOIN blunder_fixes as bf
                ON b.id = bf.blunder_id
            WHERE bf.blunder_id IS NULL AND
                  b.enabled IS NULL
            LIMIT 100000
            """
        )

        blunders = connection.cursor.fetchall()
        return blunders

def getBlundersFromPacks(timePerPosition):
    with db.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT  b.id,
                    b.fen_before,
                    b.blunder_move,
                    b.forced_line,
                    b.elo
            FROM blunders as b
            INNER JOIN pack_blunders as pb
                ON b.id = pb.blunder_id
            LEFT JOIN blunder_fixes as bf
                ON b.id = bf.blunder_id
            WHERE bf.blunder_id IS NULL AND
                  b.enabled IS NULL
            ORDER BY RANDOM()
            """
        )

        blunders = connection.cursor.fetchall()
        return blunders

def getBlundersFromTags(timePerPosition):
    with db.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT  b.id,
                    b.fen_before,
                    b.blunder_move,
                    b.forced_line,
                    b.elo
            FROM blunders as b
            INNER JOIN blunder_tags as bt
                ON b.id = bt.blunder_id
            LEFT JOIN blunder_fixes as bf
                ON b.id = bf.blunder_id
            WHERE bf.blunder_id IS NULL AND
                  b.enabled IS NULL
            ORDER BY RANDOM()
            LIMIT 100000
            """
        )

        blunders = connection.cursor.fetchall()
        return blunders

def getSingleBlunder(id):
    with db.PostgreConnection('r') as connection:
        connection.cursor.execute("""
            SELECT  b.id,
                    b.fen_before,
                    b.blunder_move,
                    b.forced_line,
                    b.elo
            FROM blunders as b
            WHERE b.id = '%s'
            """ % (id,)
        )

        blunders = connection.cursor.fetchall()
        return blunders

def saveBlunder(blunder_id, forced_line, elo, changed, meta):
    with db.PostgreConnection('w') as connection:
        connection.cursor.execute("""
            INSERT INTO blunder_fixes(blunder_id, forced_line, elo, changed, meta)
            VALUES ('%s', '%s'::text[], %s, %s, '%s'::jsonb)
            """ % (blunder_id, as_array(forced_line), elo, changed, json.dumps(meta))
        )

def main():
    #logging.basicConfig(level=logging.DEBUG)

    timePerPosition = 6000
    blunderGap = 220

    engine = "/third-party/stockfish-8-linux/Linux/stockfish_8_x64"
    engine_path = "%s%s" % (os.path.dirname(__file__),engine)
    with analyze.PVBuilder(engine_path) as builder:
        builder.setTime(timePerPosition)
        builder.setBlunderGap(blunderGap)

        blunders = getBlunders(timePerPosition)
        #blunders = getTopScoreBlunders()
        #blunders = getBlundersFromPacks(timePerPosition)
        #blunders = getBlundersFromTags(timePerPosition)
        #blunders = getSingleBlunder('557d85dae13823b81e8b02d1')

        print("Got %s records from server, begining to analyze" % len(blunders))
        for index, blunder in enumerate(blunders):
            try:
                print("######################## %s / %s ########################" % (index, len(blunders)))
                blunder_id, fenBefore, blunderMove, oldPv, elo = blunder
                print("Calculating blunder id: %s - %s %s" % (blunder_id, fenBefore, blunderMove))
                moves, newElo = builder.start(fenBefore, blunderMove)

                newPv = [
                        move['1']['move']
                        for move in moves[1:]
                ]

                print("New: (%s) %s" % (newElo, newPv))
                print("Old: (%s) %s" % (elo, oldPv))

                meta = {
                        'msec': timePerPosition,
                        'moves': moves[1:]
                }

                try:
                    changed = 1 if newPv != oldPv else 0
                    saveBlunder(blunder_id, newPv, newElo, changed, meta)
                except Exception as e:
                    print("Probably duplicate?")
                    print(e)
            except analyze.Mate0Exception as e:
                print(e)
                print("We got Mate in 0 exception. Skipping this puzzle for now...")
        print("All blunders analyzed...")

if __name__ == '__main__':

    while True:
        try:
            main()
        except Exception as e:
            print("Some strange error happened... ")
            time.sleep(10)
                
