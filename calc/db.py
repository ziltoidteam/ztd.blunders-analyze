import psycopg2

host="198.7.57.237"
user="postgres"
dbname="chessdb"
password="chessdb"

class PostgreConnection:
    def __init__(self, type):
        self.type = type
        assert type in 'rw'

        self.connection = psycopg2.connect('host=%s dbname=%s user=%s password=%s' % (host,dbname,user,password))
        self.cursor = self.connection.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exception, _1, _2):
        if self.type == 'w':
            if exception is None:
                self.connection.commit()
            else:
                self.connection.rollback()

        self.cursor.close()
        self.connection.close()
